import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailComponent } from './detail/detail.component';
import { UserComponent } from './user/user.component';
import { DetailaddComponent } from './detailadd/detailadd.component';
import { DetaileditComponent } from './detailedit/detailedit.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    UserComponent,
    DetailaddComponent,
    DetaileditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
