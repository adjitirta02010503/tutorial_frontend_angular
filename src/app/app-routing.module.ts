import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetaileditComponent } from './detailedit/detailedit.component';
import { DetailComponent } from './detail/detail.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
