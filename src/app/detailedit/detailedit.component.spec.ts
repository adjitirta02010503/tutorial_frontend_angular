import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaileditComponent } from './detailedit.component';

describe('DetaileditComponent', () => {
  let component: DetaileditComponent;
  let fixture: ComponentFixture<DetaileditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetaileditComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DetaileditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
