import { TestBed } from '@angular/core/testing';

import { DetaileditService } from './detailedit.service';

describe('DetaileditService', () => {
  let service: DetaileditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetaileditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
