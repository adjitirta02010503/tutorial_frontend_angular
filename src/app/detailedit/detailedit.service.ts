import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DetailEditModel } from './detailEditModel';

@Injectable({
  providedIn: 'root'
})
export class DetaileditService {

  url = "http://127.0.0.1:8000/api/bmi"

  constructor(
    private httpClient: HttpClient
  ) { }

  updateData(data : DetailEditModel){
    return this.httpClient.put(this.url, data)
  }

}
