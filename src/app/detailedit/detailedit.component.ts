import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DetaileditService } from './detailedit.service';
import { DetailEditModel } from './detailEditModel';

@Component({
  selector: 'app-detailedit',
  templateUrl: './detailedit.component.html',
  styleUrl: './detailedit.component.css'
})
export class DetaileditComponent implements OnInit {

  @ViewChild('closeBtn')
  closeBtn!: ElementRef<HTMLElement>;
  isDetailEdit: boolean = true;
  @Output() afterSave = new EventEmitter<boolean>();
  @Input() id: any;

  constructor(
    private detailEditService: DetaileditService
  ){}


  form = new DetailEditModel

  ngOnInit(): void {
    this.form = {
      id: this.id.id,
      name: this.id.name,
      umur: this.id.umur,
      alamat: this.id.alamat,
      jenis_kelamin: this.id.jenis_kelamin,
      hobby: this.id.hobby,
      berat: this.id.berat,
      tinggi: this.id.tinggi,
    }
  }

  toggleUpdate(item: any){
    this.isDetailEdit = !this.isDetailEdit
    this.id = item.id
  }

  updateData(){
    this.detailEditService.updateData(this.form).subscribe(res=>{
      console.log(res)
      this.close()
      this.afterSave.emit();
    })
  }

  close() {
    let el: HTMLElement = this.closeBtn.nativeElement;
    el.click();
  }
}
