import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DetailDeleteModel } from './detailDeleteModel';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor(
    private httpClient: HttpClient
  ) { }

  httpOptions : any
  url = 'http://127.0.0.1:8000/api/bmi'

  getDataDetail(){
    return this.httpClient.get(this.url, this.httpOptions)
  }

  deleteData(data : DetailDeleteModel){
    return this.httpClient.delete(this.url+'/'+data)
  }
}
