import { Component, OnDestroy, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { DetailService } from './detail.service';
import { DetailDeleteModel } from './detailDeleteModel';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.css'
})
export class DetailComponent implements OnInit,OnDestroy {
  title = 'DETAIL COMPONENT'
  name = 'ADJI WIDHI TIRTA'
  hobby = ['Makan','Masak','Ngomik','Ngoding']
  isDetailAdd = false;
  isDetailEdit = false;
  counter = 0
  id : number = 0;
  resStringFy : string = 'Data Telah Terhhapus'
  model = new DetailDeleteModel()

  columTable = ['NAMA','ALAMAT','UMUR','HOBBY','GENDER','TINGGI','BERAT','BERAT IDEAL','','']

  user = {
    name: this.name,
    hobby: this.hobby,
    age:18,
    gender: "male",
    height: 162,
    weight: 55,
    addres: "Malang city"
  }

  getName(){
    return this.name
  }

  dataDetail : any

  constructor(
    private detailService : DetailService
  ){
    console.log("DETAIL CONSTRUKTOR DIPANGGIL");
  }

  ngOnInit(): void {
    this.getDataDetail()
    // setInterval(()=>{
    //   this.counter
    //   console.log(this.counter);

    // },1000)
    console.log("DETAIL ngOnInit DIPANGGIL");
  }

  getDataDetail(){
    this.detailService.getDataDetail().subscribe(res => {
      this.dataDetail = res
      console.log(res)
    })
  }

  ngOnDestroy(): void {
    clearInterval
    console.log("DETAIL ngOnDestroy DIPANGGIL");
  }

  toggleUser(){
    this.isDetailAdd = !this.isDetailAdd
  }

  toggleUpdate(item: any){
    this.isDetailEdit = !this.isDetailEdit
    this.id = item
  }

  deleteData(id : DetailDeleteModel){
    this.detailService.deleteData(id).subscribe(res => {
      this.resStringFy = JSON.stringify(res)
      Swal.fire({
        title: "Berhasil",
        text: "Data berhasil di hapus",
        icon: "success"
      });
      this.getDataDetail()
    })
  }

}
