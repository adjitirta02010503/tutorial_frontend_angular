import { Injectable } from '@angular/core';
import { DetailAddModel } from './detailAddModel';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DetailaddService {

  constructor(
    private httpClient: HttpClient
  ) { }

  url = "http://127.0.0.1:8000/api/bmi"

  insertData(data : DetailAddModel){
    return this.httpClient.post(this.url, data)
  }
}
