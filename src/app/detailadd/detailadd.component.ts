import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { DetailAddModel } from './detailAddModel';
import { DetailaddService } from './detailadd.service';

@Component({
  selector: 'app-detailadd',
  templateUrl: './detailadd.component.html',
  styleUrl: './detailadd.component.css'
})
export class DetailaddComponent {

  isDetailadd = true;
  @ViewChild('closeBtn')
  closeBtn!: ElementRef<HTMLElement>;
  @Output() afterSave = new EventEmitter<boolean>();

  toggleUser(){
    this.isDetailadd = !this.isDetailadd
  }

  form = new DetailAddModel()

  constructor(
    private detailAddService: DetailaddService
  ){}

  insertData(){
    this.detailAddService.insertData(this.form).subscribe(res=>{
      console.log(res)
      this.close()
      this.afterSave.emit();
    })
  }

  close() {
    let el: HTMLElement = this.closeBtn.nativeElement;
    el.click();
  }

}
