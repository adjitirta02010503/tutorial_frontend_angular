import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailaddComponent } from './detailadd.component';

describe('DetailaddComponent', () => {
  let component: DetailaddComponent;
  let fixture: ComponentFixture<DetailaddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetailaddComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DetailaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
