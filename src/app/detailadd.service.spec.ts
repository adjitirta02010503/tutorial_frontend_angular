import { TestBed } from '@angular/core/testing';

import { DetailaddService } from './detailadd.service';

describe('DetailaddService', () => {
  let service: DetailaddService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailaddService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
