import { Component } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent {

  isUser = true;

  toggleUser(){
    this.isUser = !this.isUser
  }
}
