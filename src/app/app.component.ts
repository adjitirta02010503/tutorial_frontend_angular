import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent implements OnInit {
  title = 'belajar';
  isDetail = false;

  constructor(){
    console.log("APP CONSTRUKTOR DIPANGGIL");

  }

  ngOnInit(): void {
    console.log("APP ngOnInit DIPANGGIL");
  }

  toggleDetail(){
    this.isDetail = !this.isDetail
  }

}
